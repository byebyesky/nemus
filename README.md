# NEmuS
NEmuS is a WIP NES emulator written in C++.  

## Motivation
I'd like to preface this one with: I have no idea what I'm exactly doing and learn everything as I go.  
I've been interested in the inner workings of computers and everything around them for quite a while
and I wanted to learn a lot more about them so what better way to learn than learning something by doing it?  
So I started writing this NES Emulator.

## Compiling
I don't currently have any good solution to compile the emulator but you can try to run `make`.