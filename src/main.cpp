#include <iostream>
#include <bitset>
#include "Cpu.h"

int main(int argc, char** argv) {
    if(argc > 1) {
        printf("%s\n", argv[1]);
    } else {
        printf("Please specify an argument!\n");
    }

    Cpu cpu;
    cpu.execute(0x1);
    //cpu.execute(0xFF);

    cpu.print_debug();
    cpu.set_overflow(128);
    cpu.print_debug();

    return 0;
}