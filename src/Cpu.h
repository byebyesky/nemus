#ifndef CPU_H
#define CPU_H

#define CFLAG       0x1
#define ZFLAG       0x2
#define IFLAG       0x4
#define DFLAG       0x8
#define BFLAG       0x10
#define ALWAYS_ONE  0x20
#define OFLAG       0x40
#define NFLAG       0x80

#include <stdint.h>

class Cpu {
    public:
        Cpu();
        void print_debug();
        void init();
        int fetch();
        void execute(uint8_t opcode);

        void set_carry(uint16_t operand);
        void set_zero(uint16_t operand);
        void set_interrupt(uint16_t status);
        void set_decimal(uint16_t status);
        void set_b(uint16_t status);
        void set_overflow(uint16_t operand);
        void set_negative(uint16_t operand);

        void ora(uint16_t operand);

    private:
        uint8_t A, X, Y, P, SP = 0;						//Accumulator, X, Y, Flags, StackPointer					
        uint16_t PC;					            //ProgramCounter
        uint8_t memory[0xFFFF];
        uint32_t cycles;
};

#endif