#include "Cpu.h"
#include <iostream>
#include <cstring>

Cpu::Cpu() {
    init();
}

void Cpu::print_debug() {
    printf("--------------------Debug print--------------------\n");
    printf("| Register A:\t0x%x\t\tCarry flag:\t%d |\n", A, (P & CFLAG) ? 1 : 0);
    printf("| Register X:\t0x%x\t\tZero flag:\t%d |\n", X, (P & ZFLAG) ? 1 : 0);
    printf("| Register Y:\t0x%x\t\tInterrupt flag:\t%d |\n", Y, (P & IFLAG) ? 1 : 0);
    printf("| Register SP:\t0x%x\t\tDecimal flag:\t%d |\n", SP, (P & DFLAG) ? 1 : 0);
    printf("| Register PC:\t0x%x\t\tB flag flag:\t%d |\n", PC, (P & BFLAG) ? 1 : 0);
    printf("| Current Cycle:%d\t\tOverflow flag:\t%d |\n", Y, (P & OFLAG) ? 1 : 0);
    printf("| \t\t\t\tNegative flag:\t%d |\n", (P & NFLAG) ? 1 : 0);
    printf("---------------------------------------------------\n");
}

void Cpu::init() {

    memset(memory, 0x0, sizeof(memory));
    cycles = 0;
}

int Cpu::fetch() {
    return 0;
}

void Cpu::set_carry(uint16_t operand) { //Maybe buggy with SBC/CMP?
    if(operand > 0xFF) {
        P |= CFLAG;
    } else {
        P &= ~CFLAG;
    }
}

void Cpu::set_zero(uint16_t operand) {
    if(operand == 0) {
        P |= ZFLAG;
    } else {
        P &= ~ZFLAG;
    }
}

void Cpu::set_interrupt(uint16_t status) {
    if(status) {
        P |= IFLAG;
    } else {
        P &= ~IFLAG;
    }
}

void Cpu::set_decimal(uint16_t status) {
    if(status) {
        P |= DFLAG;
    } else {
        P &= ~DFLAG;
    }
}

void Cpu::set_b(uint16_t status) {
    if(status) {
        P |= BFLAG;
    } else {
        P &= ~BFLAG;
    }
}

void Cpu::set_overflow(uint16_t operand) {
    if((!(A & 1 << 7) && (operand & 1 << 7)) || ((A & 1 << 7) && !(operand & 1 << 7) )) {
        P |= OFLAG;
    } else {
        P &= ~OFLAG;
    }
}

void Cpu::set_negative(uint16_t operand) {
    if(operand & 1 << 7) {
        P |= NFLAG;
    } else {
        P &= ~NFLAG;
    }
}

void Cpu::ora(uint16_t operand) {
        operand |= A;
        set_negative(operand);
        set_zero(operand);
        A = operand;
}

//zpg - Zeropage addressing: $00XX
//abs - Absolute addressing: $xxxx

void Cpu::execute(uint8_t opcode) {
    uint16_t operand = 0x0;
    switch(opcode) {
        case 0x00:
            printf("BRK\n");
            cycles += 7;
            break;
        case 0x01:
            printf("ORA(ind,X)\n");
            //Read operand from mem
            ora(operand);
            break;
        case 0x05:
            printf("ORA zpg\n");
            break;
        case 0x06:
            printf("ASL zpg\n");
            break;
        case 0x8:
            printf("PHP\n");
            break;
        case 0x9:
            printf("ORA #\n");
            break;
        case 0xA:
            printf("ASL A");
            break;
        case 0xD:
            printf("ORA abs\n");
            break;
        case 0xE:
            printf("ASL abs\n");
            break;
        case 0x18:
            set_carry(0x0);
        case 0x38:
            set_carry(0x100);
        case 0xB8:
            printf("CLV\n");
            set_overflow(0);
        default:
            printf("Opcode 0x%x not Implemented!\n", opcode);
            print_debug();
    }
}
